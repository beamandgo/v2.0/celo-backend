﻿using System;
using CommandLine;

namespace BeamAndGo.Celo.Listener
{
    public class Options
    {
        [Option('f', "filters", HelpText = "List of filters to match when parsing inputs. Separate multiple with comma. Filters are matched with OR condition.", Default = "StableToken")]
        public string Filters { get; set; }

        [Option('v', "verbose", Required = false, Default = false, HelpText = "Show verbose output messages.")]
        public bool Verbose { get; set; }

        [Option('w', "webhook", Required = true, HelpText = "The webhook URL.")]
        public string Webhook { get; set; }

        [Option('a', "account", Required = true, HelpText = "The recipient account.")]
        public string Account { get; set; }

        [Option('t', "timeout", Required = false, Default = 60, HelpText = "Idle timeout before sending an alert.")]
        public int Timeout { get; set; }

        [Option('r', "retry", Required = false, Default = 5, HelpText = "Maximum number of retries on a failed webhook.")]
        public int MaxRetries { get; set; }

        [Option('i', "interval", Required = false, Default = 15, HelpText = "Minimum retry interval, in seconds. This will be multiplied by the number of attemts as an exponential backoff.")]
        public int RetryInterval { get; set; }

        [Option('h', "smtphost", Required = false, Default = "localhost", HelpText = "SMTP server for sending notifications.")]
        public string SmtpHost { get; set; }

        [Option("smtpport", Required = false, Default = 25, HelpText = "SMTP server port.")]
        public int SmtpPort { get; set; }

        [Option('u', "smtpuser", Required = false, Default = "", HelpText = "Username for SMTP authentication.")]
        public string SmtpUser { get; set; }

        [Option('p', "smtppass", Required = false, Default = "", HelpText = "Password for SMTP authentication.")]
        public string SmtpPass { get; set; }

        [Option('m', "mailto", Required = true, Default = "", HelpText = "Email address to receive notifications.")]
        public string MailTo { get; set; }
    }
}
