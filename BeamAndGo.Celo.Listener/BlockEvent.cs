﻿using System;
using System.Text.Json.Serialization;
using System.Numerics; // Probably not needed if we are not using BigInt

namespace BeamAndGo.Celo.Listener
{
    public class BlockEvent
    {
        [JsonPropertyName("txHash")]
        public string TxHash { get; set; }

        [JsonPropertyName("blockNumber")]
        public long BlockNumber { get; set; }

        [JsonPropertyName("blockTimestamp")]
        public DateTime BlockTimestamp { get; set; }

        [JsonPropertyName("contract")]
        public string Contract { get; set; }

        [JsonPropertyName("eventName")]
        public string EventName { get; set; }

        [JsonPropertyName("comment")]
        public string Comment { get; set; }

        [JsonPropertyName("from")]
        public string From { get; set; }

        [JsonPropertyName("to")]
        public string To { get; set; }

        [JsonPropertyName("value")]
        public string Value { get; set; }

        public decimal cUSD
        {
            get
            {
                try
                {
                    return decimal.Parse(Value) / 1000000000000000000m;  // 1e18 (eighteen zeros)
                }
                catch
                {
                    return 0;
                }
            }
        }
    }
}
