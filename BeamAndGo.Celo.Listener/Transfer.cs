﻿using System;
namespace BeamAndGo.Celo.Listener
{
    public class Transfer
    {
        // How long to keep a transfer before purging
        private const double Expiry = 300; // 300 seconds(5 minutes)

        public string TxHash { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public long BlockNumber { get; set; }
        public DateTime BlockTimestamp { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public decimal Amount { get; set; }
        public string Comment { get; set; }

        public DateTimeOffset NextRetryAttempt { get; set; }
        public int RetryAttempts { get; set; }

        public double TTL
        {
            get
            {
                return DateTime.UtcNow.Subtract(CreateTimestamp).TotalSeconds;
            }
        }

        public Transfer(BlockEvent blockEvent)
        {
            TxHash = blockEvent.TxHash;
            CreateTimestamp = DateTime.UtcNow;
            BlockNumber = blockEvent.BlockNumber;
            BlockTimestamp = blockEvent.BlockTimestamp;
            From = blockEvent.From;
            To = blockEvent.To;
            Amount = blockEvent.cUSD;
        }

        public void MergeBlockEvent(BlockEvent blockEvent)
        {
            // Merge only if TxHash and BlockNumber are the same
            if (TxHash == blockEvent.TxHash && BlockNumber == blockEvent.BlockNumber)
            {
                // Update timestamp
                BlockTimestamp = blockEvent.BlockTimestamp;

                // Merge Comment
                if (blockEvent.EventName == "TransferComment")
                {
                    Comment = blockEvent.Comment;
                }

                // Merge Transfer
                if (blockEvent.EventName == "Transfer")
                {
                    From = blockEvent.From;
                    To = blockEvent.To;
                    Amount = blockEvent.cUSD;
                }
            }
        }

        public bool IsExpired()
        {
            return TTL >= Expiry && !IsComplete();
        }

        public bool IsComplete()
        {
            return Amount > 0 &&
                !string.IsNullOrWhiteSpace(From) &&
                !string.IsNullOrWhiteSpace(To) &&
                !string.IsNullOrWhiteSpace(Comment);
        }
    }
}
