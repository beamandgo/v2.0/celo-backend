using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;
using CommandLine;
using Serilog;

namespace BeamAndGo.Celo.Listener
{
    class Program
    {
        /// <summary>
        /// Application options
        /// </summary>
        private static Options _options { get; set; }

        /// <summary>
        /// List of strings to filter on
        /// </summary>
        static readonly List<string> Filters = new List<string>();

        /// <summary>
        /// List of pending transfers
        /// </summary>
        static readonly IDictionary<string, Transfer> PendingTransfers = new ConcurrentDictionary<string, Transfer>();

        /// <summary>
        /// HTTP Client for making WebHook requests
        /// </summary>
        static readonly HttpClient HttpClient = new HttpClient();

        /// <summary>
        /// Webhook URI
        /// </summary>
        static Uri WebHookUri;

        /// <summary>
        /// Cancellation token souce for processor task
        /// </summary>
        static CancellationTokenSource CancellationTokenSource;

        /// <summary>
        /// Worker task
        /// </summary>
        static Task ProcessorTask;
        static object ProcessorTaskLock = new object();

        /// <summary>
        /// Last activity timestamp
        /// </summary>
        static DateTimeOffset LastActivity = DateTimeOffset.UtcNow;

        /// <summary>
        /// Last idle duration
        /// </summary>
        static double LastIdleDuration = 0;

        /// <summary>
        /// Flag to track notifications
        /// </summary>
        static bool NotificationSent = false;

        static void Main(string[] args)
        {
            // Parse command line args and then execute in Run()
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(Run);
        }

        static void Run(Options options)
        {
            _options = options;

            // Create a console logger
            var logConfig = new LoggerConfiguration();
            if (options.Verbose) logConfig = logConfig.MinimumLevel.Debug();
            Log.Logger = logConfig.WriteTo.Console().CreateLogger();

            try
            {
                // Add filters
                Filters.AddRange(options.Filters.Split(','));

                // Parse the webhook URI
                WebHookUri = new Uri(options.Webhook);

                // Set up cancel event handler
                Console.CancelKeyPress += Exit;

                // Start worker task
                CancellationTokenSource = new CancellationTokenSource();
                ProcessorTask = Task.Run(() => { ProcessEvents(CancellationTokenSource.Token); }, CancellationTokenSource.Token);

                Log.Information("Ready, awaiting input...");

                while (true)
                {
                    var line = Console.ReadLine();

                    if (line == null)
                    {
                        Log.Information("Got EOF, exiting.");
                        break;
                    }
                    else if (line.Length > 0)
                    {
                        Log.Debug($"Read line: {line}");

                        lock (ProcessorTaskLock)
                        {
                            LastIdleDuration = DateTimeOffset.UtcNow.Subtract(LastActivity).TotalSeconds;
                            LastActivity = DateTimeOffset.UtcNow;
                        }

                        if (MatchFilters(line))
                        {
                            try
                            {
                                // Attempt to parse the event JSON line
                                var blockEvent = JsonSerializer.Deserialize<BlockEvent>(line);

                                if ((blockEvent.EventName == "Transfer" && blockEvent.To == options.Account) || // We only want transfers to our account
                                     blockEvent.EventName == "TransferComment") // Or all comment events (because they do not carry the account)
                                {
                                    NewEvent(blockEvent);
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Warning(ex, "Error parsing JSON.");
                            }
                        }
                        else
                        {
                            Log.Debug($"Filters not matched, skipping line.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Unhandled exception.");
            }
            finally
            {
                Exit(null, null);
            }
        }

        private static void Exit(object sender, ConsoleCancelEventArgs e)
        {
            CancellationTokenSource.Cancel();
            ProcessorTask.Wait();
            CancellationTokenSource.Dispose();
        }

        private static bool MatchFilters(string line)
        {
            foreach (var filter in Filters)
            {
                if (!line.Contains(filter))
                {
                    // There are more non matches than matches, so looking for a non match is more efficient
                    return false;
                }
            }

            return true;
        }

        private static void NewEvent(BlockEvent blockEvent)
        {

            // Check if the key exists
            if (PendingTransfers.ContainsKey(blockEvent.TxHash))
            {
                // Merge events with the same hash
                PendingTransfers[blockEvent.TxHash].MergeBlockEvent(blockEvent);
                Log.Information($"Merged in cache: txHash={blockEvent.TxHash} block={blockEvent.BlockNumber} timestamp={blockEvent.BlockTimestamp} comment={blockEvent.Comment} from={blockEvent.From} to={blockEvent.To} cUSD=${blockEvent.cUSD} ({blockEvent.Value})");
            }
            else
            {
                // Add to list of pending transfers
                PendingTransfers.Add(blockEvent.TxHash, new Transfer(blockEvent));
                Log.Information($"Added to cache: txHash={blockEvent.TxHash} block={blockEvent.BlockNumber} timestamp={blockEvent.BlockTimestamp} comment={blockEvent.Comment} from={blockEvent.From} to={blockEvent.To} cUSD=${blockEvent.cUSD} ({blockEvent.Value})");
            }
        }

        private static void Notify(string subject, string body)
        {
            try
            {
                var client = new SmtpClient(_options.SmtpHost, _options.SmtpPort);

                client.SendCompleted += NotifyCompleted;

                if (!string.IsNullOrWhiteSpace(_options.SmtpUser))
                {
                    client.Credentials = new NetworkCredential(_options.SmtpUser, _options.SmtpPass);
                    client.EnableSsl = true;
                }

                var message = new MailMessage
                {
                    From = new MailAddress(_options.MailTo),
                    Subject = $"{Dns.GetHostName()}: {subject}",
                    Body = body,
                    IsBodyHtml = false,
                };

                message.To.Add(new MailAddress(_options.MailTo));

                Log.Information($"Sending notification: to={_options.MailTo} server={client.Host}:{client.Port} subject={message.Subject}");

                client.SendAsync(message, message);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error sending notification email.");
            }
        }

        private static void NotifyCompleted(object sender, AsyncCompletedEventArgs e)
        {
            var message = (MailMessage)e.UserState;

            if (e.Cancelled)
            {
                Log.Warning($"Notification cancelled: to={message.To} subject={message.Subject}");
            }

            if (e.Error != null)
            {
                Log.Error($"Notification error: {e.Error}");
            }
            else
            {
                Log.Information($"Notification sent: to={message.To} subject={message.Subject}");
            }
        }

        private static void ProcessEvents(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                foreach (var transfer in PendingTransfers.Values)
                {                    
                    if (transfer.IsComplete() && (transfer.NextRetryAttempt == null || transfer.NextRetryAttempt < DateTimeOffset.UtcNow))
                    {
                        // Increment attempts first
                        transfer.RetryAttempts++;

                        // Delay
                        var delay = _options.RetryInterval * transfer.RetryAttempts;

                        // Calculate next retry date/time with exponential backoff by using interval x attempts
                        transfer.NextRetryAttempt = DateTime.UtcNow.AddSeconds(delay);

                        if (transfer.RetryAttempts > _options.MaxRetries)
                        {
                            Log.Error($"Maximum number of retries reached: txHash={transfer.TxHash} timestamp={transfer.BlockTimestamp} comment={transfer.Comment} from={transfer.From} to={transfer.To} cUSD=${transfer.Amount}");
                            Notify($"Maximum number of retries reached: txHash={transfer.TxHash}", $"TxHash: {transfer.TxHash}\nComment: {transfer.Comment}\nFrom: {transfer.From}\nTo: {transfer.To}\ncUSD: {transfer.Amount}");

                            PendingTransfers.Remove(transfer.TxHash);
                            Log.Information($"Removed from cache: txHash={transfer.TxHash} block={transfer.BlockNumber} timestamp={transfer.BlockTimestamp} comment={transfer.Comment} from={transfer.From} to={transfer.To} cUSD=${transfer.Amount}");
                        }
                        else
                        {
                            try
                            {
                                Log.Information($"Calling webhook: uri={WebHookUri} txHash={transfer.TxHash} timestamp={transfer.BlockTimestamp} comment={transfer.Comment} from={transfer.From} to={transfer.To} cUSD=${transfer.Amount}");
                                var webhookResponse = HttpClient.PostAsJsonAsync<Transfer>(WebHookUri, transfer).GetAwaiter().GetResult();

                                if (webhookResponse.IsSuccessStatusCode)
                                {
                                    Log.Information($"Webhook success: uri={WebHookUri} txHash={transfer.TxHash} attempt={transfer.RetryAttempts} status={webhookResponse.StatusCode}");
                                    PendingTransfers.Remove(transfer.TxHash);
                                    Log.Information($"Removed from cache: txHash={transfer.TxHash} block={transfer.BlockNumber} timestamp={transfer.BlockTimestamp} comment={transfer.Comment} from={transfer.From} to={transfer.To} cUSD=${transfer.Amount}");
                                }
                                else
                                {
                                    var result = webhookResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                                    Log.Warning($"Webhook failed: uri={WebHookUri} txHash={transfer.TxHash} attempt={transfer.RetryAttempts} status={webhookResponse.StatusCode} response={result}");
                                    Notify($"Webhook failed: txHash={transfer.TxHash} attempt={transfer.RetryAttempts}", $"TxHash: {transfer.TxHash}\nComment: {transfer.Comment}\nStatus: {webhookResponse.StatusCode}\n{result}");
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex, $"Webhook exception: uri={WebHookUri} txHash={transfer.TxHash} attempt={transfer.RetryAttempts}");
                                Notify($"Webhook failed: txHash={transfer.TxHash} attempt={transfer.RetryAttempts}", $"TxHash: {transfer.TxHash}\nComment: {transfer.Comment}\nException: {ex.Message}\n{ex.StackTrace}");
                            }
                        }
                    }
                    else if (transfer.IsExpired())
                    {
                        PendingTransfers.Remove(transfer.TxHash);
                        Log.Information($"Expired from cache: txHash={transfer.TxHash} block={transfer.BlockNumber} timestamp={transfer.BlockTimestamp} comment={transfer.Comment} from={transfer.From} to={transfer.To} cUSD=${transfer.Amount}");

                        // Notify if a transfer to account occurs with no comment
                        if (transfer.To == _options.Account)
                        {
                            Notify($"No comments found: txHash={transfer.TxHash}", $"TxHash: {transfer.TxHash}\nComment: {transfer.Comment}\nFrom: {transfer.From}\nTo: {transfer.To}\ncUSD: {transfer.Amount}");
                        }
                    }
                    else
                    {
                        Log.Debug($"Not yet expired: txHash={transfer.TxHash} block={transfer.BlockNumber} timestamp={transfer.BlockTimestamp} ttl={transfer.TTL}");
                    }
                }

                Thread.Sleep(1000);

                lock (ProcessorTaskLock)
                {
                    var idle = DateTimeOffset.UtcNow.Subtract(LastActivity).TotalSeconds;

                    if (idle > _options.Timeout)
                    {
                        if (!NotificationSent)
                        {
                            Log.Error($"Idle timeout: {idle} seconds");
                            Notify("Idle timeout: Check node!", $"No activity detected for {idle} seconds. Check if node is alive!");
                            NotificationSent = true;
                        }
                    }
                    else if (NotificationSent)
                    {
                        Log.Information($"Node has recovered: after {LastIdleDuration} seconds");
                        Notify("Node has recovered.", $"Activity resumed after {LastIdleDuration} seconds. We are back in action.");
                        NotificationSent = false;
                    }
                }
            }

            Log.Debug($"Processor task ended.");
        }
    }
}
