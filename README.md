# BeamAndGo Celo Transaction Listener

This is a simple console app which parses the output from the `eksportisto` tool,
searches for transactions with comments and fires off a webhook.

## Architecture

Celo Node --> Celo Eksportisto --> BeamAndGo Celo Listener --> Webhook

This depends on two external components to be built:
* Celo Node
* Celo Eksportisto

## Building

Requirements:
* Docker
* DotNet SDK 3.1

To build the eksportisto tool:

	cd eksportisto
	make build

To build BeamAndGo.Celo.Listener:

	cd BeamAndGo Celo Listener
	dotnet build

## Running

All three components must be run in the following order:
1. Celo Node
2. Celo eksportisto
3. BeamAndGo Celo Listener

To run the Celo Node:

	cd celo
	make up # to run in the background
	make down # to stop the container
	make run # to run interactively (for debugging only)

Tip: If the node is consuming too much CPU and making the system
unresponsive, consider changing the priority of the process, e.g.

	ps -ef | grep geth # get the PID of the process
	renice -n 10 -p $(PID)

To run eksportisto:

	cd eksportisto
	make up # to run in the background
	make down # to stop the container
	make run # to run interactively (for debugging only)

To run the listener:

	./listen.sh --webhook https://your.server.com/path/to/webhook --account 0x1234567890ABCDEF1234567890ABCDEF12345678

It is recommended to run the listener as a background process or in a `screen` session.

## Wiki

For further reading, see:
https://wiki.beamandgo.com/display/DEV/Celo

## License

Proprietary; Copyright 2020, BeamAndGo Pte Ltd.

